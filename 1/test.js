const RGB = {
    red: "#FF0000",
    green: "#00FF00",
    blue: "#0000FF"
};

const WB = {
    white: "#FFFFFF",
    black: "#000000"
};

var colors = {};

Object.assign(colors, RGB, WB);

console.log(RGB);
console.log(WB);
console.log(colors);

