wait(0);

function wait(i){
    if (i < 5) {
        setTimeout(function () {
            console.log(i);
            wait(i + 1);
        }, 1000);
    }
}
