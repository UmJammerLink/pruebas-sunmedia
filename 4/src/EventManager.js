import Event from './Event';

export default class EventManager{

    constructor (types, events){
        this.types  = types;
        this.events = events;
        }

    run(time) {
        var self = this;
        setTimeout(function(){
            self.events.forEach(function(elementEvent) {
                let event = new Event(elementEvent.second, elementEvent.type, elementEvent.message);
                if (time === elementEvent.second) {
                    self.types.forEach(function(elementType){
                        if(elementType === elementEvent.type){
                            console.log('At second ' + time + ': {type: "' + elementEvent.type + '", message: "'  + elementEvent.message + '"}');
                        }
                    });
                }
            });
            self.run(time + 1);
        }, 1000);
    }
};